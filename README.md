# Form query language (Fql)

Heavily WIP expression parser and interpreter.

```typescript
import { FqlInterpreter } from './dsl/fql-interpreter';
import { parseFql } from './dsl/fql-parser';
import { FormContext } from './models/form-context';

const context: FormContext = {
  formFields: [
    {
      name: 'title',
      type: 'remark',
      value: 'Maintenance report'
    },
    {
      name: 'location',
      type: 'remark',
      value: 'Somewhere in the world'
    }
  ]
};
const inputText = `title = 'Maintenance report'`;
const parseResult = parseFql(inputText);

const interpreter = new FqlInterpreter(context);
const result = interpreter.visit(parseResult.cst);
```