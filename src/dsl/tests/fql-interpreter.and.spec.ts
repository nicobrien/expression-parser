import * as fc from 'fast-check';
import {
  expressionsThatShouldBeFalse,
  expressionsThatShouldBeTrue
} from './test-data';
import { interpret } from './test-interpret';

describe(`'and' operator`, () => {
  it(`should have higher precendence than 'or' on the left`, () => {
    const result = interpret('false and true or true');
    expect(result.value).toEqual(true);
  });

  it(`should have higher precendence than 'or' on the right`, () => {
    const result = interpret('true or true and false');
    expect(result.value).toEqual(true);
  });

  it('should interpret the conjunction of any true expressions as true', () => {
    fc.assert(
      fc.property(
        fc.constantFrom(...expressionsThatShouldBeTrue),
        fc.constantFrom(...expressionsThatShouldBeTrue),
        (expression1: string, expression2: string) => {
          const result = interpret(`(${expression1}) and (${expression2})`);

          expect(result.lexErrors).toHaveLength(0);
          expect(result.parseErrors).toHaveLength(0);
          expect(result.value).toEqual(true);
        }
      ),
      { numRuns: 20 }
    );
  });

  it('should interpret the conjunction of any false expressions as false', () => {
    fc.assert(
      fc.property(
        fc.constantFrom(...expressionsThatShouldBeFalse),
        fc.constantFrom(...expressionsThatShouldBeFalse),
        (expression1: string, expression2: string) => {
          const result = interpret(`(${expression1}) and (${expression2})`);

          expect(result.lexErrors).toHaveLength(0);
          expect(result.parseErrors).toHaveLength(0);
          expect(result.value).toEqual(false);
        }
      ),
      { numRuns: 20 }
    );
  });

  it('should interpret the conjunction of any true expressions with a false expression as false', () => {
    fc.assert(
      fc.property(
        fc.constantFrom(...expressionsThatShouldBeTrue),
        fc.constantFrom(...expressionsThatShouldBeFalse),
        (expression1: string, expression2: string) => {
          const result = interpret(`(${expression1}) and (${expression2})`);

          expect(result.lexErrors).toHaveLength(0);
          expect(result.parseErrors).toHaveLength(0);
          expect(result.value).toEqual(false);
        }
      ),
      { numRuns: 20 }
    );
  });
});
