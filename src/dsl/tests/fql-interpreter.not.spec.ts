import { interpret } from './test-interpret';

describe(`'not' operator`, () => {
  it(`should have higher precendence than 'or'`, () => {
    const result = interpret('not false or true');
    expect(result.value).toEqual(true);
  });

  it(`should have higher precendence than 'and'`, () => {
    const result = interpret('not true and false');
    expect(result.value).toEqual(false);
  });
});
