import * as fc from 'fast-check';
import { interpret } from './test-interpret';

describe(`'in' operator`, () => {
  it('should return true if matching field has value in string array literal', () => {
    const result = interpret(`notef3c.value in ['X_1', 'X_2', 'X_3']`, {
      formFields: [
        {
          key: 'notef3c',
          name: 'note',
          type: 'remark',
          value: 'X_2'
        }
      ]
    });
    expect(result.lexErrors).toHaveLength(0);
    expect(result.parseErrors).toHaveLength(0);
    expect(result.value).toEqual(true);
  });

  it('should return false if matching field has value that is not in string array literal', () => {
    const result = interpret(`title90c.value in ['a', 'b', 'c', 'e']`, {
      formFields: [
        {
          key: 'title90c',
          name: 'title',
          type: 'remark',
          value: 'd'
        }
      ]
    });
    expect(result.lexErrors).toHaveLength(0);
    expect(result.parseErrors).toHaveLength(0);
    expect(result.value).toEqual(false);
  });

  it('should return true if field has value in arbitrary string literal array', () => {
    fc.assert(
      fc.property(
        fc.record({
          index: fc.nat({ max: 19 }),
          array: fc.array(fc.hexaString(), { minLength: 20, maxLength: 20 })
        }),
        (o: { index: number; array: string[] }) => {
          fc.pre(o.index > 0);

          const formattedArray =
            '[' + o.array.map((s) => `'${s}'`).join(', ') + ']';
          const result = interpret(`title619.value in ${formattedArray}`, {
            formFields: [
              {
                key: 'title619',
                name: 'title',
                type: 'remark',
                value: o.array[o.index]
              }
            ]
          });
          expect(result.lexErrors).toHaveLength(0);
          expect(result.parseErrors).toHaveLength(0);
          expect(result.value).toEqual(true);
        }
      ),
      { numRuns: 20 }
    );
  });

  it('should thing', () => {
    const formattedArray = `['','','','','','','','','','','','','','','','','','','','']`;
    const result = interpret(`title619.value in ${formattedArray}`, {
      formFields: [
        {
          key: 'title619',
          name: 'title',
          type: 'remark',
          value: ''
        }
      ]
    });
    expect(result.lexErrors).toHaveLength(0);
    expect(result.parseErrors).toHaveLength(0);
    expect(result.value).toEqual(true);
  });

  it('should return false if field has value not in arbitrary string literal array', () => {
    fc.assert(
      fc.property(
        fc.hexaString(),
        fc.array(fc.hexaString(), { minLength: 20, maxLength: 20 }),
        (stringNotInArray: string, array: string[]) => {
          fc.pre(array.indexOf(stringNotInArray) === -1);

          const formattedArray =
            '[' + array.map((s) => `'${s}'`).join(', ') + ']';
          const result = interpret(`title223.value in ${formattedArray}`, {
            formFields: [
              {
                key: 'title223',
                name: 'title',
                type: 'remark',
                value: stringNotInArray
              }
            ]
          });
          expect(result.lexErrors).toHaveLength(0);
          expect(result.parseErrors).toHaveLength(0);
          expect(result.value).toEqual(false);
        }
      ),
      { numRuns: 20 }
    );
  });
});
