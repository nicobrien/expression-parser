import * as fc from 'fast-check';
import { interpret } from './test-interpret';

describe(`'=' operator`, () => {
  it('should be able to access nested property for comparison', () => {
    const result = interpret(`keyf3c.value = 'spectacular'`, {
      formFields: [
        {
          key: `keyf3c`,
          name: 'key',
          type: 'remark',
          value: 'spectacular'
        }
      ]
    });
    expect(result.lexErrors).toHaveLength(0);
    expect(result.parseErrors).toHaveLength(0);
    expect(result.value).toEqual(true);
  });

  it.each([
    { key: 'a', value: 'a' },
    { key: 'X', value: 'Y' },
    { key: 'key', value: 'value' },
    { key: 'number', value: '1234' },
    { key: 'symbols', value: '!@#$%^&*()_+=-{}[]' },
    { key: 'title', value: 'The long dark' },
    { key: 'location', value: '22 c. Acme drive, Imaginary s. EARTH' },
    { key: 'quote', value: '"This is a test", said james.' }
  ])(
    'should return true given %j and matching field name/value pair in form context',
    ({ key, value }) => {
      const result = interpret(`${key}f21 = '${value}'`, {
        formFields: [
          {
            key: `${key}f21`,
            name: key,
            type: 'remark',
            value: value
          }
        ]
      });
      expect(result.lexErrors).toHaveLength(0);
      expect(result.parseErrors).toHaveLength(0);
      expect(result.value).toEqual(true);
    }
  );

  it('should return false given matching name is found but value does not match', () => {
    const key = 'key';
    fc.assert(
      fc.property(
        fc.hexaString(),
        fc.hexaString(),
        (v1: string, v2: string) => {
          fc.pre(v1 !== v2);

          const result = interpret(`${key}a59 = '${v1}'`, {
            formFields: [
              {
                key: `${key}a59`,
                name: key,
                type: 'remark',
                value: v2
              }
            ]
          });
          expect(result.lexErrors).toHaveLength(0);
          expect(result.parseErrors).toHaveLength(0);
          expect(result.value).toEqual(false);
        }
      )
    );
  });
});
