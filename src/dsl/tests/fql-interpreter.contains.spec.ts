import { interpret } from './test-interpret';

describe(`'~' operator`, () => {
  it('should return true if matching field has value containing string literal', () => {
    const result = interpret(`notef3c.value ~ 'inspection'`, {
      formFields: [
        {
          key: 'notef3c',
          name: 'note',
          type: 'remark',
          value: 'the inspection is due on Monday'
        }
      ]
    });
    expect(result.lexErrors).toHaveLength(0);
    expect(result.parseErrors).toHaveLength(0);
    expect(result.value).toEqual(true);
  });

  it('should return false if matching field has value that does not contain string literal', () => {
    const result = interpret(`title1a8.value ~ 'planned'`, {
      formFields: [
        {
          key: 'title1a8',
          name: 'title',
          type: 'remark',
          value: 'Maintenance & planning'
        }
      ]
    });
    expect(result.lexErrors).toHaveLength(0);
    expect(result.parseErrors).toHaveLength(0);
    expect(result.value).toEqual(false);
  });
});
