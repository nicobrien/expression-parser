import { allTokens, Identifier } from '../fql-tokens';

describe('Fql Tokens', () => {
  /**
   * The Identifier must appear after the keywords because all keywords are
   * valid identifiers.
   */
  it(`should order 'Identifier' last in the tokens array`, () => {
    expect(allTokens[allTokens.length - 1]).toEqual(Identifier);
  });
});
