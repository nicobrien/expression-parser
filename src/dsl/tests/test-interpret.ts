import { FormContext } from '../../models/form-context';
import { FqlInterpreter } from '../fql-interpreter';
import { getFqlLexer } from '../fql-lexer';
import { FqlParser } from '../fql-parser';

// TODO - this should be part of the api and not test specific

const parser = new FqlParser();

export const interpret = (
  s: string,
  context: FormContext = { formFields: [] }
) => {
  const interpreter = new FqlInterpreter(context);
  const lexResult = getFqlLexer().tokenize(s);
  parser.input = lexResult.tokens;
  const cst = parser.expression();
  const value = interpreter.visit(cst);
  return {
    value: value,
    lexErrors: lexResult.errors,
    parseErrors: parser.errors
  };
};
