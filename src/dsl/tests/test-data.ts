export const expressionsThatShouldBeTrue = [
  'true',
  'not false',
  'true and true',
  'true or false',
  'false or true',
  'not (false and true)',
  'true and (true or false)',
  'true and (false or true)',
  '(true or false) and true',
  '(false or true) and true',
  'false or (true and true)',
  '(true and true) or false'
];

export const expressionsThatShouldBeFalse = [
  'false',
  'not true',
  'false and true',
  'true and false',
  'false or false',
  'not (true or false)',
  'false and (true or false)',
  'false and (false or true)',
  '(true or false) and false',
  '(false or true) and false',
  'false or (true and false)',
  '(false and true) or false'
];
