import * as fc from 'fast-check';
import {
  expressionsThatShouldBeFalse,
  expressionsThatShouldBeTrue
} from './test-data';
import { interpret } from './test-interpret';

describe(`'or' operator`, () => {
  it('should interpret the disjunction of any true expressions as true', () => {
    fc.assert(
      fc.property(
        fc.constantFrom(...expressionsThatShouldBeTrue),
        fc.constantFrom(...expressionsThatShouldBeTrue),
        (expression1: string, expression2: string) => {
          const result = interpret(`(${expression1}) or (${expression2})`);

          expect(result.lexErrors).toHaveLength(0);
          expect(result.parseErrors).toHaveLength(0);
          expect(result.value).toEqual(true);
        }
      ),
      { numRuns: 20 }
    );
  });

  it('should interpret the disjunction of any false expressions as false', () => {
    fc.assert(
      fc.property(
        fc.constantFrom(...expressionsThatShouldBeFalse),
        fc.constantFrom(...expressionsThatShouldBeFalse),
        (expression1: string, expression2: string) => {
          const result = interpret(`(${expression1}) or (${expression2})`);

          expect(result.lexErrors).toHaveLength(0);
          expect(result.parseErrors).toHaveLength(0);
          expect(result.value).toEqual(false);
        }
      ),
      { numRuns: 20 }
    );
  });

  it('should interpret the disjunction of any true expressions with a false expression as true', () => {
    fc.assert(
      fc.property(
        fc.constantFrom(...expressionsThatShouldBeTrue),
        fc.constantFrom(...expressionsThatShouldBeFalse),
        (expression1: string, expression2: string) => {
          const result = interpret(`(${expression1}) or (${expression2})`);

          expect(result.lexErrors).toHaveLength(0);
          expect(result.parseErrors).toHaveLength(0);
          expect(result.value).toEqual(true);
        }
      ),
      { numRuns: 20 }
    );
  });
});
