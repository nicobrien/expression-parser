import {
  expressionsThatShouldBeFalse,
  expressionsThatShouldBeTrue
} from './test-data';
import { interpret } from './test-interpret';

describe('Fql Grammar', () => {
  describe('basic expression', () => {
    it.each(expressionsThatShouldBeTrue)(
      `should interpret '%s' as true`,
      (s: string) => {
        const result = interpret(s);

        expect(result.lexErrors).toHaveLength(0);
        expect(result.parseErrors).toHaveLength(0);
        expect(result.value).toEqual(true);
      }
    );

    it.each(expressionsThatShouldBeFalse)(
      `should interpret '%s' as false`,
      (s: string) => {
        const result = interpret(s);

        expect(result.lexErrors).toHaveLength(0);
        expect(result.parseErrors).toHaveLength(0);
        expect(result.value).toEqual(false);
      }
    );
  });

  describe('expression combinations', () => {
    it('should return true when expression with two keys matches both values', () => {
      const result = interpret(
        `key1f3c.value = 'hello' and key2f3c.value = 'world'`,
        {
          formFields: [
            {
              key: 'key1f3c',
              name: 'key1',
              type: 'remark',
              value: 'hello'
            },
            {
              key: 'key2f3c',
              name: 'key2',
              type: 'remark',
              value: 'world'
            }
          ]
        }
      );
      expect(result.lexErrors).toHaveLength(0);
      expect(result.parseErrors).toHaveLength(0);
      expect(result.value).toEqual(true);
    });

    it('should return true when expression with two keys matches both values', () => {
      const result = interpret(
        `key123.value = 'hello' and key456.value = 'world'`,
        {
          formFields: [
            {
              key: 'key123',
              name: 'key1',
              type: 'remark',
              value: 'hello'
            },
            {
              key: 'key456',
              name: 'key2',
              type: 'remark',
              value: 'something that is not world'
            }
          ]
        }
      );
      expect(result.lexErrors).toHaveLength(0);
      expect(result.parseErrors).toHaveLength(0);
      expect(result.value).toEqual(false);
    });

    it('should handle nested expressions correcty', () => {
      const result = interpret(
        `key1a.value = 'hello' and (key2b.value = 'world' or key2b.value = 'parser')`,
        {
          formFields: [
            {
              key: 'key1a',
              name: 'key1',
              type: 'remark',
              value: 'hello'
            },
            {
              key: 'key2b',
              name: 'key2',
              type: 'remark',
              value: 'parser'
            }
          ]
        }
      );
      expect(result.lexErrors).toHaveLength(0);
      expect(result.parseErrors).toHaveLength(0);
      expect(result.value).toEqual(true);
    });
  });
});
