import { createToken, Lexer } from 'chevrotain';

export const LeftParenthesis = createToken({
  name: 'LeftParenthesis',
  pattern: /\(/,
  label: `'('`
});

export const RightParenthesis = createToken({
  name: 'RightParenthesis',
  pattern: /\)/,
  label: `')'`
});

export const LeftSquareBracket = createToken({
  name: 'LeftSquareBracket',
  pattern: /\[/,
  label: `'['`
});

export const RightSquareBracket = createToken({
  name: 'RightSquareBracket',
  pattern: /\]/,
  label: `']'`
});

export const Identifier = createToken({
  name: 'Identifier',
  pattern: /[a-zA-Z][a-zA-Z0-9]*/
});

export const StringLiteral = createToken({
  name: 'StringLiteral',
  pattern: /'([^'\\]|\\[\s\S])*'/
});

export const Whitespace = createToken({
  name: 'Whitespace',
  pattern: /[ \t\n\r]+/,
  group: Lexer.SKIPPED
});

export const Comma = createToken({
  name: 'Comma',
  pattern: /,/,
  label: `','`
});

export const FullStop = createToken({
  name: 'FullStop',
  pattern: /\./,
  label: `'.'`
});

export const EqualsOperator = createToken({
  name: 'EqualsOperator',
  pattern: /=/,
  label: `'='`
});

export const ContainsOperator = createToken({
  name: 'ContainsOperator',
  pattern: /~/,
  label: `'~'`
});

export const InOperator = createToken({
  name: 'InOperator',
  pattern: /in/,
  longer_alt: Identifier,
  label: `'in'`
});

export const NotOperator = createToken({
  name: 'NotOperator',
  pattern: /not/,
  longer_alt: Identifier,
  label: `'not'`
});

export const AndOperator = createToken({
  name: 'AndOperator',
  pattern: /and/,
  longer_alt: Identifier,
  label: `'and'`
});

export const OrOperator = createToken({
  name: 'OrOperator',
  pattern: /or/,
  longer_alt: Identifier,
  label: `'or'`
});

export const TrueLiteral = createToken({
  name: 'TrueLiteral',
  pattern: /true/,
  label: `'true'`
});

export const FalseLiteral = createToken({
  name: 'FalseLiteral',
  pattern: /false/,
  label: `'false'`
});

/**
 * The order of TokenTypes in the lexer definition matters.
 *
 * If we place keywords before the Identifier than identifiers that have a
 * keyword like prefix will not be lexer correctly. For example, the keyword
 * "or" and the identifier "orange".  The token vector would be ["or", "ange"]
 * instead of ["orange"].
 *
 * On the other hand if we place keywords after the Identifier than they will
 * never be lexed as keywords as all keywords are usually valid identifiers.
 *
 * The solution is to place the keywords BEFORE the Identifier with an added
 * property telling the lexer to prefer the longer identifier alternative if one
 * is found.
 */
export const allTokens = [
  // note we are placing WhiteSpace first as it is very
  // common thus it will speed up the lexer.
  Whitespace,

  Comma,
  FullStop,
  LeftParenthesis,
  RightParenthesis,
  LeftSquareBracket,
  RightSquareBracket,
  StringLiteral,
  InOperator,
  EqualsOperator,
  ContainsOperator,
  NotOperator,
  AndOperator,
  OrOperator,
  TrueLiteral,
  FalseLiteral,

  // The Identifier must appear after the keywords
  // because all keywords are valid identifiers.
  Identifier
];
