import { Lexer } from 'chevrotain';
import { allTokens } from './fql-tokens';

export const getFqlLexer = () => new Lexer(allTokens);
