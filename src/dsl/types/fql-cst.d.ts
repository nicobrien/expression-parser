import type { CstNode, ICstVisitor, IToken } from "chevrotain";

export interface ExpressionCstNode extends CstNode {
  name: "expression";
  children: ExpressionCstChildren;
}

export type ExpressionCstChildren = {
  orExpression: OrExpressionCstNode[];
};

export interface OrExpressionCstNode extends CstNode {
  name: "orExpression";
  children: OrExpressionCstChildren;
}

export type OrExpressionCstChildren = {
  lhs: AndExpressionCstNode[];
  OrOperator?: IToken[];
  rhs?: AndExpressionCstNode[];
};

export interface AndExpressionCstNode extends CstNode {
  name: "andExpression";
  children: AndExpressionCstChildren;
}

export type AndExpressionCstChildren = {
  lhs: NotExpressionCstNode[];
  AndOperator?: IToken[];
  rhs?: NotExpressionCstNode[];
};

export interface NotExpressionCstNode extends CstNode {
  name: "notExpression";
  children: NotExpressionCstChildren;
}

export type NotExpressionCstChildren = {
  negation?: IToken[];
  rhs: AtomExpressionCstNode[];
};

export interface AtomExpressionCstNode extends CstNode {
  name: "atomExpression";
  children: AtomExpressionCstChildren;
}

export type AtomExpressionCstChildren = {
  TrueLiteral?: IToken[];
  FalseLiteral?: IToken[];
  parenthesisExpression?: ParenthesisExpressionCstNode[];
  relationExpression?: RelationExpressionCstNode[];
};

export interface ParenthesisExpressionCstNode extends CstNode {
  name: "parenthesisExpression";
  children: ParenthesisExpressionCstChildren;
}

export type ParenthesisExpressionCstChildren = {
  LeftParenthesis: IToken[];
  expression: ExpressionCstNode[];
  RightParenthesis: IToken[];
};

export interface IdentifierExpressionCstNode extends CstNode {
  name: "identifierExpression";
  children: IdentifierExpressionCstChildren;
}

export type IdentifierExpressionCstChildren = {
  Identifier: IToken[];
  FullStop?: IToken[];
};

export interface RelationExpressionCstNode extends CstNode {
  name: "relationExpression";
  children: RelationExpressionCstChildren;
}

export type RelationExpressionCstChildren = {
  identifierExpression: IdentifierExpressionCstNode[];
  inExpression?: InExpressionCstNode[];
  containsExpression?: ContainsExpressionCstNode[];
  comparisonExpression?: ComparisonExpressionCstNode[];
};

export interface ArrayExpressionCstNode extends CstNode {
  name: "arrayExpression";
  children: ArrayExpressionCstChildren;
}

export type ArrayExpressionCstChildren = {
  LeftSquareBracket: IToken[];
  StringLiteral?: IToken[];
  Comma?: IToken[];
  RightSquareBracket: IToken[];
};

export interface InExpressionCstNode extends CstNode {
  name: "inExpression";
  children: InExpressionCstChildren;
}

export type InExpressionCstChildren = {
  InOperator: IToken[];
  arrayExpression: ArrayExpressionCstNode[];
};

export interface ComparisonExpressionCstNode extends CstNode {
  name: "comparisonExpression";
  children: ComparisonExpressionCstChildren;
}

export type ComparisonExpressionCstChildren = {
  EqualsOperator: IToken[];
  StringLiteral: IToken[];
};

export interface ContainsExpressionCstNode extends CstNode {
  name: "containsExpression";
  children: ContainsExpressionCstChildren;
}

export type ContainsExpressionCstChildren = {
  ContainsOperator: IToken[];
  StringLiteral: IToken[];
};

export interface ICstNodeVisitor<IN, OUT> extends ICstVisitor<IN, OUT> {
  expression(children: ExpressionCstChildren, param?: IN): OUT;
  orExpression(children: OrExpressionCstChildren, param?: IN): OUT;
  andExpression(children: AndExpressionCstChildren, param?: IN): OUT;
  notExpression(children: NotExpressionCstChildren, param?: IN): OUT;
  atomExpression(children: AtomExpressionCstChildren, param?: IN): OUT;
  parenthesisExpression(children: ParenthesisExpressionCstChildren, param?: IN): OUT;
  identifierExpression(children: IdentifierExpressionCstChildren, param?: IN): OUT;
  relationExpression(children: RelationExpressionCstChildren, param?: IN): OUT;
  arrayExpression(children: ArrayExpressionCstChildren, param?: IN): OUT;
  inExpression(children: InExpressionCstChildren, param?: IN): OUT;
  comparisonExpression(children: ComparisonExpressionCstChildren, param?: IN): OUT;
  containsExpression(children: ContainsExpressionCstChildren, param?: IN): OUT;
}
