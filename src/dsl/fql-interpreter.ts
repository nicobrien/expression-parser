import { tokenMatcher } from 'chevrotain';
import { FormContext } from '../models/form-context';
import { FqlParser } from './fql-parser';
import * as Token from './fql-tokens';
import {
  ExpressionCstChildren,
  OrExpressionCstChildren,
  AndExpressionCstChildren,
  AtomExpressionCstChildren,
  ParenthesisExpressionCstChildren,
  ComparisonExpressionCstChildren,
  NotExpressionCstChildren,
  ContainsExpressionCstChildren,
  ArrayExpressionCstChildren,
  InExpressionCstChildren,
  IdentifierExpressionCstChildren,
  IdentifierExpressionCstNode,
  RelationExpressionCstChildren
} from './types/fql-cst';

const parser = new FqlParser();

// Obtains the default CstVisitor constructor to extend.
const BaseCstVisitor = parser.getBaseCstVisitorConstructor();

// All our semantics go into the visitor, completly separated from the grammar.
export class FqlInterpreter extends BaseCstVisitor {
  public constructor(
    private readonly context: FormContext = { formFields: [] }
  ) {
    super();
    this.validateVisitor();
  }

  public expression(ctx: ExpressionCstChildren) {
    // visiting an array is equivalent to visiting its first element.
    return this.visit(ctx.orExpression);
  }

  // Note the usage of the 'rhs' and 'lhs' labels to increase the readability.
  public orExpression(ctx: OrExpressionCstChildren) {
    let result = this.visit(ctx.lhs);

    // 'rhs' key may be undefined as the grammar defines it as optional
    // (MANY === zero or more)
    if (ctx.rhs) {
      ctx.rhs.forEach((rhsOperand, idx) => {
        // there will be one operator for each rhs operand
        const rhsValue = this.visit(rhsOperand);

        const operator = ctx.OrOperator;
        if (operator) {
          if (tokenMatcher(operator[idx], Token.OrOperator)) {
            result ||= rhsValue;
          }
        }
      });
    }
    return result;
  }

  public andExpression(ctx: AndExpressionCstChildren) {
    let result = this.visit(ctx.lhs);
    if (ctx.rhs) {
      ctx.rhs.forEach((rhsOperand, idx) => {
        const rhsValue = this.visit(rhsOperand);

        const operator = ctx.AndOperator;
        if (operator) {
          if (tokenMatcher(operator[idx], Token.AndOperator)) {
            result &&= rhsValue;
          }
        }
      });
    }
    return result;
  }

  public notExpression(ctx: NotExpressionCstChildren) {
    const result = this.visit(ctx.rhs);
    return ctx.negation ? !result : result;
  }

  public atomExpression(ctx: AtomExpressionCstChildren) {
    if (ctx.parenthesisExpression) {
      return this.visit(ctx.parenthesisExpression);
    } else if (ctx.relationExpression) {
      return this.visit(ctx.relationExpression);
    } else if (ctx.TrueLiteral) {
      return true;
    } else if (ctx.FalseLiteral) {
      return false;
    }
  }

  public relationExpression(ctx: RelationExpressionCstChildren) {
    const nestedValue = this.visit(ctx.identifierExpression);
    if (ctx.comparisonExpression) {
      return this.visit(ctx.comparisonExpression, nestedValue);
    }
    if (ctx.containsExpression) {
      return this.visit(ctx.containsExpression, nestedValue);
    }
    if (ctx.inExpression) {
      return this.visit(ctx.inExpression, nestedValue);
    }
  }

  public identifierExpression(ctx: IdentifierExpressionCstChildren) {
    const keys = ctx.Identifier.map((token) => token.image);
    const matchingField = this.context.formFields.find(
      (fieldContext) => fieldContext.key === keys[0]
    );
    if (!matchingField) {
      return false;
    }
    const nestedProperty =
      keys.length > 1
        ? this.getNestedValue(matchingField, keys.splice(1))
        : matchingField.value;
    return nestedProperty;
  }

  // TODO
  //  - other comparisons
  //  - type checking
  //  - default values
  //  - compile time checks,
  //  - e.t.c

  public inExpression(ctx: InExpressionCstChildren, value: any) {
    const array = ctx.arrayExpression[0].children.StringLiteral?.reduce(
      (acc, node) => {
        return node ? [...acc, this.stripQuotes(node.image)] : acc;
      },
      <string[]>[]
    );

    return (
      value !== undefined &&
      array?.length !== undefined &&
      array.indexOf(value) != -1
    );
  }

  public comparisonExpression(
    ctx: ComparisonExpressionCstChildren,
    value: any
  ) {
    return value === this.stripQuotes(ctx.StringLiteral[0].image);
  }

  public containsExpression(ctx: ContainsExpressionCstChildren, value: any) {
    return value
      .toString()
      .includes(this.stripQuotes(ctx.StringLiteral[0].image));
  }

  public parenthesisExpression(ctx: ParenthesisExpressionCstChildren) {
    return this.visit(ctx.expression);
  }

  public arrayExpression(ctx: ArrayExpressionCstChildren) {
    return ctx.StringLiteral ? [ctx.StringLiteral[0].image] : [];
  }

  private stripQuotes(s: string) {
    return s.replace(/^'/, '').replace(/'$/, '');
  }

  private getNestedValue(obj: Record<string, any>, keys: string[]) {
    let o: Record<string, any> | undefined = obj;
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i];
      if (!o || o[key] === undefined || o[key] === null) {
        o = undefined;
        break;
      }
      o = o[key];
    }
    return o;
  }
}
