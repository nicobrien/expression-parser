import { CstParser, Rule } from 'chevrotain';
import { getFqlLexer } from './fql-lexer';
import * as Token from './fql-tokens';

export class FqlParser extends CstParser {
  public constructor() {
    super(Token.allTokens);
    this.performSelfAnalysis();
  }

  public expression = this.RULE('expression', () => {
    this.SUBRULE(this.orExpression);
  });

  // Lowest precedence thus it is first in the rule chain
  // The precedence of binary expressions is determined by how
  // far down the Parse Tree the binary expression appears

  private orExpression = this.RULE('orExpression', () => {
    this.SUBRULE(this.andExpression, { LABEL: 'lhs' });
    this.MANY(() => {
      this.CONSUME(Token.OrOperator);
      // the index "2" in SUBRULE2 is needed to identify the
      // unique position in the grammar during runtime
      this.SUBRULE2(this.andExpression, { LABEL: 'rhs' });
    });
  });

  private andExpression = this.RULE('andExpression', () => {
    this.SUBRULE(this.notExpression, { LABEL: 'lhs' });
    this.MANY(() => {
      this.CONSUME(Token.AndOperator);
      this.SUBRULE2(this.notExpression, { LABEL: 'rhs' });
    });
  });

  private notExpression = this.RULE('notExpression', () => {
    this.OPTION(() => this.CONSUME(Token.NotOperator, { LABEL: 'negation' }));
    this.SUBRULE(this.atomExpression, { LABEL: 'rhs' });
  });

  private atomExpression = this.RULE('atomExpression', () => {
    this.OR([
      { ALT: () => this.CONSUME(Token.TrueLiteral) },
      { ALT: () => this.CONSUME(Token.FalseLiteral) },
      { ALT: () => this.SUBRULE(this.parenthesisExpression) },
      { ALT: () => this.SUBRULE(this.relationExpression) }
    ]);
  });

  private parenthesisExpression = this.RULE('parenthesisExpression', () => {
    this.CONSUME(Token.LeftParenthesis);
    this.SUBRULE(this.expression);
    this.CONSUME(Token.RightParenthesis);
  });

  private identifierExpression = this.RULE('identifierExpression', () => {
    this.AT_LEAST_ONE_SEP({
      SEP: Token.FullStop,
      DEF: () => this.CONSUME2(Token.Identifier)
    });
  });

  private relationExpression = this.RULE('relationExpression', () => {
    this.SUBRULE(this.identifierExpression);
    this.OR([
      { ALT: () => this.SUBRULE(this.inExpression) },
      { ALT: () => this.SUBRULE(this.containsExpression) },
      { ALT: () => this.SUBRULE(this.comparisonExpression) }
    ]);
  });

  private arrayExpression = this.RULE('arrayExpression', () => {
    this.CONSUME(Token.LeftSquareBracket);
    this.MANY_SEP({
      SEP: Token.Comma,
      DEF: () => this.CONSUME2(Token.StringLiteral)
    });
    this.CONSUME(Token.RightSquareBracket);
  });

  private inExpression = this.RULE('inExpression', () => {
    this.CONSUME(Token.InOperator);
    this.SUBRULE(this.arrayExpression);
  });

  private comparisonExpression = this.RULE('comparisonExpression', () => {
    this.CONSUME(Token.EqualsOperator);
    this.CONSUME(Token.StringLiteral);
  });

  private containsExpression = this.RULE('containsExpression', () => {
    this.CONSUME(Token.ContainsOperator);
    this.CONSUME(Token.StringLiteral);
  });
}

export const getProductions = (): Record<string, Rule> => {
  const parser = new FqlParser();
  return parser.getGAstProductions();
};

export const parseFql = (text: string) => {
  const lexResult = getFqlLexer().tokenize(text);

  const parser = new FqlParser();
  // setting a new input will RESET the parser instance's state
  parser.input = lexResult.tokens;

  // any top level rule may be used as an entry point
  const cst = parser.expression();

  return {
    // this is a pure grammar, the value will be undefined
    // until we add embedded actions or enable automatic CST creation
    cst: cst,
    lexErrors: lexResult.errors,
    parseErrors: parser.errors
  };
};
