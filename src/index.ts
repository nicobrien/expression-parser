import { FqlInterpreter } from './dsl/fql-interpreter';
import { parseFql } from './dsl/fql-parser';
import { FormContext } from './models/form-context';

const context: FormContext = {
  formFields: [
    {
      key: 'title6aac',
      name: 'title',
      type: 'remark',
      value: 'Maintenance report'
    },
    {
      key: 'locatione2b4',
      name: 'location',
      type: 'remark',
      value: 'c'
    }
  ]
};
const inputText = `title6aac.value ~ 'zzz' or locatione2b4.value in ['c', 'b']`;
const parseResult = parseFql(inputText);

if (
  parseResult.lexErrors.length === 0 &&
  parseResult.parseErrors.length === 0
) {
  const interpreter = new FqlInterpreter(context);
  const result = interpreter.visit(parseResult.cst);
  console.log(
    `for query |${inputText}|\nand context\n${JSON.stringify(
      context,
      null,
      2
    )},\nthe result is ${result}`
  );
} else {
  console.log(
    'There was a problem:\n',
    parseResult.lexErrors,
    parseResult.parseErrors
  );
}
