export interface FormContext {
  formFields: {
    key: string;
    name: string;
    type: 'remark' | 'metric'; // e.t.c.
    value: string | { value: number; unit: string }; // e.t.c.
  }[];
}
