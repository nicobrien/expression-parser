const path = require('path')
const fs = require('fs')
const chevrotain = require('chevrotain')
const { FqlParser } = require('../dist/dsl/fql-parser');

const parserInstance = new FqlParser();
const serializedGrammar = parserInstance.getSerializedGastProductions()

const htmlText = chevrotain.createSyntaxDiagramsCode(serializedGrammar)

const outPath = path.resolve(__dirname, '../')
fs.writeFileSync(outPath + '/generated-diagrams.html', htmlText)