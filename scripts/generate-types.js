/**
 * This is a minimal script that generates TypeScript definitions
 * from a Chevrotain parser.
 */
const { writeFileSync } = require('fs');
const { resolve } = require('path');
const { generateCstDts } = require('chevrotain');
const { getProductions } = require('../dist/dsl/fql-parser');

const dtsString = generateCstDts(getProductions());
const dtsPath = resolve(__dirname, '..', 'src', 'dsl', 'types', 'fql-cst.d.ts');
writeFileSync(dtsPath, dtsString);
